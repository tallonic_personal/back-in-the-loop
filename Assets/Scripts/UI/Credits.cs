﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Credits : MonoBehaviour
{
    List<Transform> CreditTextObjects;
    Transform Logo;

    void Awake()
    {
        CreditTextObjects = new List<Transform>();
        CreditTextObjects.Add(transform.Find("Credits/Credits"));
        CreditTextObjects.Add(transform.Find("Credits/Credits_2"));
        Logo = transform.Find("Credits/TallonicLogo");
    }

    public IEnumerator ShowCredits()
    {
        yield return new WaitForSeconds(0.5f);

        var textObjects = CreditTextObjects
            .Select(_ => (textMesh: _.gameObject.GetComponent<TMPro.TextMeshProUGUI>(), FadeController: _.gameObject.GetComponent<FadeController>())).ToArray();
        var logoFadeController = Logo.gameObject.GetComponent<FadeController>();
        Coroutine fadeCoroutine = null;

        var credits = GetCreditsDataList();
        foreach (var creditData in credits)
        {
            textObjects.ToList().ForEach(_ => _.textMesh.fontSize = creditData.FontSize);

            textObjects[0].textMesh.text = creditData.Text1;
            fadeCoroutine = StartCoroutine(textObjects[0].FadeController.FadeOut(creditData.FadeOut));
            if (!creditData.ShowTextsTogether) yield return fadeCoroutine;

            textObjects[1].textMesh.text = creditData.Text2;
            fadeCoroutine = StartCoroutine(textObjects[1].FadeController.FadeOut(creditData.FadeOut));
            if (!creditData.ShowTextsTogether) yield return fadeCoroutine;

            if (creditData.ShowLogo)
            {
                fadeCoroutine = StartCoroutine(logoFadeController.FadeOut(creditData.FadeOut));
                if (!creditData.ShowTextsTogether) yield return fadeCoroutine;
            }

            yield return new WaitForSeconds(creditData.Duration);

            fadeCoroutine = StartCoroutine(textObjects[0].FadeController.FadeIn(creditData.FadeIn));
            if (!creditData.ShowTextsTogether) yield return fadeCoroutine;

            fadeCoroutine = StartCoroutine(textObjects[1].FadeController.FadeIn(creditData.FadeIn));
            if (!creditData.ShowTextsTogether) yield return fadeCoroutine;

            if (creditData.ShowLogo)
            {
                fadeCoroutine = StartCoroutine(logoFadeController.FadeIn(creditData.FadeOut));
                if (!creditData.ShowTextsTogether) yield return fadeCoroutine;
            }

            if (creditData.ShowTextsTogether)
            {
                yield return fadeCoroutine;
            }
        }
    }

    CreditData[] GetCreditsDataList() => new List<CreditData>
    {
        CreditData.New("CREDITS", duration: 3.25f, fontSize: 50f),
        CreditData.New("PROGRAMMING", "C.MARCOVICH"),
        CreditData.New("GAME DESIGN", "C.MARCOVICH"),
        CreditData.New("GRAPHICS / ARTWORK", "KENNEY"),
        CreditData.New("MUSIC", "\nKevin MacLeod\nINCOMPETECH"),
        CreditData.New("A GAME BY", "\nTALLONIC", 50f, true, fadeOut: 2f),
        CreditData.New(null, fadeIn: 0f, fadeOut: 0f, duration: 2f),
        CreditData.New(text2: "THANK YOU\nFOR\nPLAYING!", fontSize: 50f, duration: 3f, fadeOut: 4f, fadeIn: 2f)
    }.ToArray();

    private class CreditData
    {
        public string Text1 { get; private set; }
        public string Text2 { get; private set; }
        public float FontSize { get; private set; }
        public bool ShowLogo { get; private set; }
        public float FadeIn { get; private set; }
        public float FadeOut { get; private set; }
        public float Duration { get; private set; }
        public bool ShowTextsTogether { get; set; }

        public CreditData(string text1, string text2, float fontSize, bool showLogo, float fadeOut, float fadeIn,
            float duration, bool showTextsTogether = false)
        {
            Text1 = text1;
            Text2 = text2;
            FontSize = fontSize;
            ShowLogo = showLogo;
            FadeOut = fadeOut;
            FadeIn = fadeIn;
            Duration = duration;
            ShowTextsTogether = showTextsTogether;
        }

        public static CreditData New(string text1 = null, string text2 = null, float fontSize = 30f, bool showLogo = false, float fadeOut = 1f, float fadeIn = 0.5f,
            float duration = 1.75f, bool fadeTextsTogether = false) =>
            new(text1, text2, fontSize, showLogo, fadeOut, fadeIn, duration, fadeTextsTogether);
    }
}