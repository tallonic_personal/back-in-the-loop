﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design.Serialization;
using Assets.Helpers;
using UnityEngine;

public class Boomer : Enemy
{
    public SpinChaseAbility SPIN_CHASE;
    
    private Coroutine _isAttacking;
    
    new void Start () {
        base.Start ();
        SPIN_CHASE.SetSpinner(GetComponent<Spinner>());
        _sm.SetState (Move);
    }

    void Move () {
        _moveCounter -= Time.deltaTime * 5f;

        if (_moveCounter <= 0) {
            _sm.SetState (Search);
            SetRandomShootCounter ();
        }
    }

    void Search () {
        if (_player == null) {
            _sm.SetState (Move);
            return;
        }
        var xDiff = transform.position.x - _player.transform.position.x;
        if (xDiff.IsBetween (-0.02f, 0.02f))
            _sm.SetState (Attack);
    }

    void Attack () {
        if (!_pathing.IsStopped () && _isAttacking == null)
            _isAttacking = StartCoroutine (Attacking ());
    }

    IEnumerator Attacking () {
        _sm.SetState (Attack);

        _pathing.Stop (true);

        yield return new WaitForSeconds (0.25f);
        
        var holdPosition = transform.position;
        var preparePosition = new Vector3 (transform.position.x, transform.position.y + 0.25f, 0f);

        StartCoroutine(ExecuteRotate(SPIN_CHASE.MaximizeSpinSpeed));
        
        while (transform.position != preparePosition)
        {
            transform.position = Vector3.MoveTowards (transform.position, preparePosition, 0.5f * Time.deltaTime);
            yield return new WaitForEndOfFrame ();
        }
        
        var velocity = 0.3f;
        var bottomYPos = -7f;
        while (transform.position.y > bottomYPos) {
            if(_player == null) break;
            if (velocity <= 30f)
                velocity += 0.3f;
            var target = new Vector3 (transform.position.x, bottomYPos, 0f);
            transform.position = Vector3.MoveTowards (transform.position, target, velocity * Time.deltaTime);
            yield return new WaitForEndOfFrame ();
        }
        while (transform.position.y < holdPosition.y) {
            if (velocity > 1f)
                velocity -= 0.3f;
            transform.position = Vector3.MoveTowards (transform.position, holdPosition, velocity * Time.deltaTime);
            yield return new WaitForEndOfFrame ();
        }
        
        yield return ExecuteRotate(SPIN_CHASE.MinimizeSpinSpeed);
        yield return FixRotationRoutine();

        yield return new WaitForSeconds (1f);

        _sm.SetState (Move);
        _pathing.Stop (false);
        _isAttacking = null;
    }

    private IEnumerator ExecuteRotate(System.Func<bool> rotateAction)
    {
        yield return new WaitUntil(rotateAction);
    }

    private IEnumerator FixRotationRoutine()
    {
        var rotCorretion = 360 * (transform.rotation.z.IsBetween (0f, 180f) ? 1f : -1);
        yield return new WaitUntil (() => {
            transform.Rotate (Vector3.forward * Time.deltaTime * rotCorretion);
            return transform.rotation.z.IsBetween (-0.1f, 0.1f);
        });
        var rot = transform.rotation;
        rot.z = 0f;
        transform.rotation = rot;
    }
}