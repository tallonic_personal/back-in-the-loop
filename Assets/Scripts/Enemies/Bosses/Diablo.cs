using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Enviroment;
using UnityEngine;
using Random = UnityEngine.Random;

public class Diablo : Boss {
    Transform _leftGun;
    Transform _rightGun;
    Transform _baseGun;

    public Transform BaseGun => _baseGun;
    public bool IsDead => dead;

    [Header ("Bullet Wall")]
    public SuperShootAbility BULLET_WALL;
    public SuperShootAbility BARRAGE;

    [Header ("Morph")]
    public MorphAbility MORPH;
    public float SpeedMod;
    int morphLevel = 0;
    float initialHealth;

    [Header ("Invisibility")]
    public Invisibility INVISIBILITY;
    public float InvisibleDuration;
    bool invisibilityEnabled;

    [Header ("Vamp Ball")]
    public BigShootAbility VAMP_BALL;

    [Header ("Gravity")]
    public BigShootAbility GRAVITY;

    [Header("Phase 2")] 
    public Phase2Ability PHASE2;
    
    [Header ("Phase 2 - Minions")]
    public SpawnMinions[] PHASE2_SPAWN_MINIONS;

    public bool deathAnimationPlayed;

    // public float gr_MinCooldown;
    // public float gr_MaxCooldown;
    // public float gr_Delay;
    // public GameObject gr_object;
    // public float gr_MaxSize;
    // public float gr_Speed;
    // float _gr_Cooldown;

    new void Start () {
        base.Start ();

        ConfigureAbilities ();
        Abilities.ForEach (_ => _.Cooldown.ResetCooldown ());
        _gun = transform.Find ("BigCannon/_gun");
        _baseGun = transform.Find ("BigCannon");
        _leftGun = transform.Find ("Weapon_Left/_gun");
        _rightGun = transform.Find ("Weapon_Right/_gun");

        initialHealth = Health;
    }

    void FixedUpdate ()
    {
        if (dead) return;
        if (PHASE2.IsEnabled)
        {
            // if (Health <= initialHealth * .5 && morphLevel == 0)
            //     _sm.SetState(Morph);
            if (Health <= initialHealth * .35f && morphLevel == 1)
                _sm.SetState(Morph);

            if (!invisibilityEnabled && Health <= initialHealth * .35f && morphLevel == 2)
                Invisibility();
        }
    }

    /* -------------------- */
    /* Basic Gun - Override */
    /* -------------------- */
    void NewGun () {
        StartCoroutine (this.NewGun_Fire ());
        _sm.SetState (Idle);
    }
    IEnumerator NewGun_Fire () {
        for (int i = 0; i < BASIC_GUN.ShootsPerBurst; i++)
        {
            LookAtPlayer ();

            var direction = _gun.up.normalized;
            Fire (_leftGun, direction);
            Fire (_rightGun, direction);

            yield return new WaitForSeconds (BASIC_GUN.TimeBetweenShoots);
        }
    }

    /* ---------- */
    /* BulletWall */
    /* ---------- */

    void BulletWall () {
        _pathing.Stop (true);
        if (_isIdle) {
            _isIdle = false;
            StartCoroutine (BulletWall_Fire ());
        }
    }
    IEnumerator BulletWall_Fire () {
        //Move To Center
        yield return new WaitUntil (() => _pathing.MoveToCenter ());

        //Delay
        yield return new WaitForSeconds (BULLET_WALL.GeneralDelay);

        //Fire
        var turn = false;
        var angle = 0;

        for (int i = 0; i < BULLET_WALL.ShootsPerBurst; i++) {
            angle = !turn ? Random.Range (-55, -45) : Random.Range (-45, -35);

            for (int r = angle; r <= -angle; r += 20) {
                LookAtBottom ();
                _leftGun.rotation = _leftGun.rotation * Quaternion.Euler (0f, 0f, r);
                var leftGunDirection = _leftGun.up.normalized;

                _rightGun.rotation = _rightGun.rotation * Quaternion.Euler (0f, 0f, r);
                var rightGunDirection = _rightGun.up.normalized;

                Fire (_leftGun, leftGunDirection, false);
                Fire (_rightGun, rightGunDirection, false);
            }

            AudioSource.PlayClipAtPoint (projectileSound, Camera.main.transform.position, projectileSoundVolume);

            yield return new WaitForSeconds (BULLET_WALL.TimeBetweenShoots);

            turn = !turn;
        }

        //Finish
        _pathing.Stop (false);
        _isIdle = true;
        _sm.SetState (Idle);
    }

    /* ---------- */
    /* Barrage */
    /* ---------- */
    void Barrage () {
        _pathing.Stop (true);
        if (_isIdle) {
            _isIdle = false;
            StartCoroutine (Barrage_Fire ());
        }
    }

    IEnumerator Barrage_Fire()
    {
        //Move To Center
        yield return new WaitUntil (() => _pathing.MoveToCenter ());

        //Delay
        yield return new WaitForSeconds (BULLET_WALL.GeneralDelay);
        
        //Fire
        var turn = false;
        var initialAngle = -50f;
        var endAngle = 50f;
        var angle = 0f;
        bool invertDirection = false;

        var timeElapsed = 0f;
        var lastFireTime = 0f;
        
        while (true)
        {
            LookAtBottom ();

            if (!invertDirection)
                angle = Mathf.Lerp(initialAngle, endAngle, timeElapsed);
            else
                angle = Mathf.Lerp(endAngle, initialAngle , timeElapsed);
            
            _leftGun.rotation = _leftGun.rotation * Quaternion.Euler (0f, 0f, angle);
            _rightGun.rotation = _rightGun.rotation * Quaternion.Euler (0f, 0f, -angle);
            
            var leftGunDirection = _leftGun.up.normalized;
            var rightGunDirection = _rightGun.up.normalized;

            if (Time.time > lastFireTime + BARRAGE.TimeBetweenShoots)
            {
                Fire(_leftGun, leftGunDirection, false);
                Fire(_rightGun, rightGunDirection, false);
                AudioSource.PlayClipAtPoint(projectileSound, Camera.main.transform.position, projectileSoundVolume);

                lastFireTime = Time.time;
            }

            if (angle == endAngle)
            {
                invertDirection = true;
                timeElapsed = 0f;
                
                //Finish
                _pathing.Stop (false);
                _isIdle = true;
                _sm.SetState (BulletWall);
            }
            if (invertDirection && angle == initialAngle) break;
            
            timeElapsed += Time.deltaTime;
            
            yield return 0f;
        }
    }
    
    /* ----- */
    /* Morph */
    /* ----- */
    void Morph () {
        _pathing.Stop (true);
        if (_isIdle) {
            _isIdle = false;
            StartCoroutine (Morph_Exec ());
        }
    }
    IEnumerator Morph_Exec () {
        morphLevel++;

        if (morphLevel == 2)
        {
            var starfield = FindFirstObjectByType<Starfield>();
            starfield.ChangeStarfieldToBossMode2();
        }
        
        var middleCollider = GetComponent<PolygonCollider2D> ();
        var leftWingCollider = transform.Find("Wing_Left").GetComponent<PolygonCollider2D> ();
        var rightWingCollider = transform.Find("Wing_Right").GetComponent<PolygonCollider2D> ();

        middleCollider.enabled = false;
        leftWingCollider.enabled = false;
        rightWingCollider.enabled = false;

        //Preparation
        var leftWingDest = Vector3.zero;
        var rightWingDest = Vector3.zero;

        var childMaterials = new List<Material> () {
            transform.Find ("Base").GetComponent<SpriteRenderer> ().material,
            transform.Find ("Wing_Left").GetComponent<SpriteRenderer> ().material,
            transform.Find ("Wing_Right").GetComponent<SpriteRenderer> ().material,
        };

        //Move To Center
        yield return new WaitUntil (_pathing.MoveToCenter);

        yield return new WaitForSeconds (MORPH.GeneralDelay);

        //ShineOn
        yield return new WaitUntil (() => ChangeAllColor (childMaterials, MORPH.MorphAlphaVariation, MORPH.MaxAlphaValue));

        //FirstMovement
        leftWingDest = _leftWing.localPosition + new Vector3 (-0.15f, 0f);
        rightWingDest = _rightWing.localPosition + new Vector3 (0.15f, 0f);

        yield return new WaitUntil (() => {
            var result = false;
            result = _pathing.MoveChildToPosition (_leftWing, leftWingDest, MORPH.MorphSpeedRatio);
            result = _pathing.MoveChildToPosition (_rightWing, rightWingDest, MORPH.MorphSpeedRatio) && result;
            return result;
        });

        //ShineOff
        yield return new WaitUntil (() => ChangeAllColor (childMaterials, -MORPH.MorphAlphaVariation, MORPH.MinAlphaValue));

        //Boost
        _pathing.MoveSpeedModifier = SpeedMod;
        yield return new WaitForSeconds (MORPH.GeneralDelay * 2);

        //Finish
        _pathing.Stop (false);
        _isIdle = true;
        _sm.SetState (Idle);

        middleCollider.enabled = true;
        leftWingCollider.enabled = true;
        rightWingCollider.enabled = true;
    }

    /* ------------ */
    /* Invisibility */
    /* ------------ */
    public void Invisibility () {
        invisibilityEnabled = true;
        StartCoroutine (SetInvisibility ());
    }
    IEnumerator SetInvisibility () {
        if (INVISIBILITY.VanishTime == 0) yield return 0;

        var sprites = new List<SpriteRenderer> ();
        foreach (Transform child in transform)
            sprites.Add (child.GetComponent<SpriteRenderer> ());

        while (true) {
            yield return new WaitUntil (() => Hidden (sprites, INVISIBILITY.VanishTime * Time.deltaTime, INVISIBILITY.MinimumAlphaVisibility));

            yield return new WaitForSeconds (InvisibleDuration);

            yield return new WaitUntil (() => Show (sprites, INVISIBILITY.VanishTime * Time.deltaTime));

            yield return new WaitForSeconds (Random.Range (INVISIBILITY.Cooldown.min_cooldown, INVISIBILITY.Cooldown.max_cooldown));
        }
    }

    /* --------- */
    /* Vamp Ball */
    /* --------- */
    void VampBall () {
        _pathing.Stop (true);
        if (_isIdle) {
            _isIdle = false;
            StartCoroutine (VampBall_Exec ());
        }
    }
    IEnumerator VampBall_Exec () {
        yield return new WaitUntil (_pathing.MoveToCenter);

        yield return new WaitForSeconds (VAMP_BALL.GeneralDelay);

        //put cannon to out
        var _baseGunDest = _baseGun.transform.localPosition + new Vector3 (0f, -0.3f);
        yield return new WaitUntil (() => _pathing.MoveChildToPosition (_baseGun, _baseGunDest, 10f));

        yield return new WaitForSeconds (VAMP_BALL.GeneralDelay);

        //create shoot
        var vampBall = Instantiate (VAMP_BALL.Projectile, _gun.transform.position + (new Vector3 (0, 0, 6)), Quaternion.identity);
        VAMP_BALL.SetShoot (vampBall);

        //Increase Ball
        yield return new WaitUntil (() => VAMP_BALL.IncreaseShoot ());
        yield return new WaitForSeconds (VAMP_BALL.GeneralDelay);

        LookAtPlayer ();
        vampBall.GetComponent<Rigidbody2D> ().velocity = _gun.up.normalized * ProjectileSpeed * 1.25f;

        yield return new WaitUntil (() => vampBall == null);

        _baseGunDest = _baseGun.transform.localPosition + new Vector3 (0f, 0.3f);

        yield return new WaitUntil (() => _pathing.MoveChildToPosition (_baseGun, _baseGunDest, 10f));

        //Finish
        _pathing.Stop (false);
        _isIdle = true;
        _sm.SetState (Idle);
    }

    /* ------- */
    /* Gravity */
    /* ------- */
    void Gravity () {
        _pathing.Stop (true);
        if (_isIdle) {
            _isIdle = false;
            StartCoroutine (Gravity_Exec ());
        }
    }
    IEnumerator Gravity_Exec () {
        yield return new WaitUntil (_pathing.MoveToCenter);

        yield return new WaitForSeconds (GRAVITY.GeneralDelay);

        var gravityField = Instantiate (GRAVITY.Projectile, transform.position + (new Vector3 (0, 0, 6)), Quaternion.identity);
        GRAVITY.SetShoot (gravityField);

        yield return new WaitUntil (() => GRAVITY.IncreaseShoot ());
        yield return new WaitForSeconds (GRAVITY.GeneralDelay);

        //Finish
        _pathing.Stop (false);
        _isIdle = true;
        _sm.SetState (Idle);
    }

    void ConfigureAbilities () {
        BULLET_WALL.Action = BulletWall;
        BARRAGE.Action = Barrage;
        MORPH.Action = Morph;
        VAMP_BALL.Action = VampBall;
        GRAVITY.Action = Gravity;
        PHASE2.Action = Phase2;

        Abilities[0].Action = NewGun;
        Abilities.Add (BULLET_WALL);
        Abilities.Add (MORPH);
        Abilities.Add (VAMP_BALL);
        Abilities.Add (GRAVITY);
        Abilities.Add (PHASE2);
        
        //Abilities.Add (BARRAGE);
        //increase movespeed
        //clones
    }

    public void SetPhase2()
    {
        if (!PHASE2.IsChanging)
        {
            PHASE2.IsChanging = true;
            _isIdle = true; //force change state
            _sm.SetState(Phase2);
        }
    }
    
    void Phase2 () {
        _pathing.Stop (true);
        if (_isIdle) {
            _isIdle = false;
            StopAllCoroutines();
            StartCoroutine (ChangeToPhase2 ());
        }
    }

    IEnumerator ChangeToPhase2()
    {
        var vortexSpawnRate = FindFirstObjectByType<VortexSpawner>().SpawnRate = 0f;
        
        var middleCollider = GetComponent<PolygonCollider2D> ();
        var leftWingCollider = transform.Find("Wing_Left").GetComponent<PolygonCollider2D> ();
        var rightWingCollider = transform.Find("Wing_Right").GetComponent<PolygonCollider2D> ();
        
        var leftWingExt = transform.Find("Wing_Left_ext");
        var rightWingExt = transform.Find("Wing_Right_ext");
        
        var childMaterials = new List<Material> () {
            transform.Find ("Base").GetComponent<SpriteRenderer> ().material,
            transform.Find ("Wing_Left").GetComponent<SpriteRenderer> ().material,
            transform.Find ("Wing_Right").GetComponent<SpriteRenderer> ().material,
            transform.Find ("Wing_Left_ext").GetComponent<SpriteRenderer> ().material,
            transform.Find ("Wing_Right_ext").GetComponent<SpriteRenderer> ().material,
            transform.Find ("Weapon_Left").GetComponent<SpriteRenderer> ().material,
            transform.Find ("Weapon_Right").GetComponent<SpriteRenderer> ().material,
            transform.Find ("BigCannon").GetComponent<SpriteRenderer> ().material
        };
        
        var backgrounds = FindObjectsByType<ScrollingBackground>(FindObjectsSortMode.None);
        var musicPlayer = FindFirstObjectByType<MusicPlayer>();
        var starfield = FindFirstObjectByType<Starfield>();
        var shipMovement = FindFirstObjectByType<PlayerController>();
        var cameraShake = GameObject.Find("Main Camera").GetComponent<CameraShake>();
        cameraShake.StopShake();
        
        middleCollider.enabled = false;
        leftWingCollider.enabled = false;
        rightWingCollider.enabled = false;
        
        //Move To Center
        musicPlayer.FadeIn(1f);

        shipMovement.MoveToCenterAnimation();
        yield return new WaitUntil (() => _pathing.MoveToPosition(new Vector3(0f, 2f, 0f)));

        yield return new WaitForSeconds (MORPH.GeneralDelay);

        yield return new WaitForSeconds(1f);
        
        //Blink
        bool blink = true;
        for (int i = 0; i < 6; i++)
        {
            ChangeAllColor(childMaterials, blink ? MORPH.MaxAlphaValue : -MORPH.MaxAlphaValue, MORPH.MaxAlphaValue);
            blink = !blink;
            yield return new WaitForSeconds(.05f);
        }
            
        //ShineOn
        for (int i = 0; i < 40; i++)
        {
            var variation = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 2f));
            var explosion = Instantiate (PHASE2.Explosion, transform.position + variation, transform.rotation);
            Destroy (explosion, durationOfExplosion);
            AudioSource.PlayClipAtPoint (PHASE2.ExplosionSfx, Camera.main.transform.position, deathSoundVolume);
            if (i > 16)
                ChangeAllColor(childMaterials, MORPH.MorphAlphaVariation, MORPH.MaxAlphaValue);
            if(i == 20)
                cameraShake.ProgressiveShake(10f, 0.05f, 1.25f);
            yield return new WaitForSeconds(.15f);
        }
        yield return new WaitUntil (() => ChangeAllColor (childMaterials, MORPH.MorphAlphaVariation, MORPH.MaxAlphaValue));
        
        //yield return new WaitForSeconds(.1f);
        
        var gravityField = Instantiate (GRAVITY.Projectile, transform.position + (new Vector3 (0, 0, 6)), Quaternion.identity);
        var gravityComp = gravityField.GetComponent<GravityField>();
        gravityComp.DontApplyGravity = true;
        gravityComp.Durability = 10f;
        GRAVITY.SetShoot(gravityField);
        gravityField.transform.localScale = new Vector3(GRAVITY.MaxSize, GRAVITY.MaxSize);

        var sr = gravityField.GetComponent<SpriteRenderer>();
        sr.sortingOrder = 1;
        var alpha = sr.color.a;
        var c = new Color(1f, 1f, 1f, 0f);
        for (int i = 0; i < 8; i++)
        {
            c.a = c.a > 0f ? 0f : 1f;
            sr.color = c;
            yield return new WaitForSeconds(.02f);
        }
        
        AudioSource.PlayClipAtPoint (PHASE2.FadeSfx, Camera.main.transform.position, deathSoundVolume);

        sr.color = new Color(.95f, .95f, .95f, 1f);

        var cv = 1f; 
        yield return new WaitUntil (() =>
        {
            var colors = new List<Color>();
            for (int i = 0; i < childMaterials.Count; i++)
            {
                var c = ChangeAlpha(childMaterials[i], -.02f);
                c.a = Mathf.Clamp(c.a, 0.5f, 1f);
                colors.Add(c);
            }

            GRAVITY.DecreaseShoot(.5f);
            var newScale = Mathf.Clamp(gravityField.transform.localScale.x, 1f, GRAVITY.MaxSize);
            gravityField.transform.localScale = Vector3.one * newScale;

            if (newScale < 35f)
            {
                cv -= .02f;
                cv = Mathf.Clamp(cv, .1f, 1f);
                sr.color = new Color(cv, cv, cv, 1f);
            }

            return newScale <= 1f && colors.All(_ => _.a <= 0.5f);
        });
        
        yield return new WaitForSeconds(1.5f);
        sr.color = Color.white;
        gravityField.transform.localScale = new Vector3(GRAVITY.MaxSize, GRAVITY.MaxSize);
        yield return new WaitForSeconds(.01f);
        gravityField.transform.localScale = new Vector3(0f, 0f);
        sr.color = new Color(cv,cv,cv,1f);

        yield return new WaitUntil(() =>
        {
            var color = ChangeAlpha(sr, .01f);
            return color.a >= 1f;
        });
        
        //ShineOff
        yield return new WaitUntil(() =>
        {
            var colors = new List<Color>();
            for (int i = 0; i < childMaterials.Count; i++)
            {
                var c = ChangeAlpha(childMaterials[i], -.01f);
                colors.Add(c);
            }
            
            cv -= .001f;
            cv = Mathf.Clamp(cv, 0f, 1f);
            sr.color = new Color(cv, cv, cv, 1f);

            return GRAVITY.IncreaseShoot(.25f) && colors.All(_ => _.a <= 0f) && cv == 0f;
        });

        //disable background
        for (int i = 0; i < backgrounds.Length; i++)
        {
            backgrounds[i].gameObject.SetActive(false);
        }

        yield return new WaitForSeconds(3f);
        
        musicPlayer.PlayEndBossTheme();
        starfield.StopParticles();

        var energySpawner = FindFirstObjectByType<EnergySpawner>();
        energySpawner.Spawn(new Vector3(0f, 7.5f, 0f), 2);
        energySpawner.Spawn(new Vector3(0f, 7.75f, 0f), 2);
        energySpawner.Spawn(new Vector3(0f, 8f, 0f), 2);

        yield return new WaitWhile(() => gravityField);
        shipMovement.EnableCollider(true);

        yield return new WaitForSeconds(1.5f);
        shipMovement.LockMovement(false);
        yield return new WaitForSeconds(1f);
        
        starfield.ChangeStarfieldToBossMode();
        yield return new WaitForSeconds(4f);
        
        //MORPH
        //ShineOn
        
        yield return new WaitUntil (() =>
        {
            var colors = new List<Color>();
            for (int i = 0; i < childMaterials.Count; i++)
            {
                var c = ChangeAlpha(childMaterials[i], .02f);
                c.a = Mathf.Clamp(c.a, 0f, 1f);
                colors.Add(c);
            }

            return colors.All(_ => _.a >= 1f);
        });
        
        yield return new WaitUntil (() => ChangeAllColor (childMaterials, MORPH.MorphAlphaVariation * .25f, MORPH.MaxAlphaValue));
        
        yield return new WaitForSeconds(.25f);
        
        var leftWingDest = Vector3.zero;
        var rightWingDest = Vector3.zero;
        
        //FirstMovement
        leftWingDest = _leftWing.localPosition + new Vector3 (-0.15f, 0f);
        rightWingDest = _rightWing.localPosition + new Vector3 (0.15f, 0f);

        yield return new WaitUntil (() => {
            var result = false;
            result = _pathing.MoveChildToPosition (_leftWing, leftWingDest, MORPH.MorphSpeedRatio * .5f);
            result = _pathing.MoveChildToPosition (_rightWing, rightWingDest, MORPH.MorphSpeedRatio * .5f) && result;
            return result;
        });
        leftWingExt.gameObject.SetActive(true);
        rightWingExt.gameObject.SetActive(true);
        
        yield return new WaitForSeconds(.25f);

        //ShineOff
        yield return new WaitUntil (() => ChangeAllColor (childMaterials, -MORPH.MorphAlphaVariation, MORPH.MinAlphaValue));
        
        
        //HEALTH
        
        Max_Health = Max_Health * 4f;
        var timeElapsed = 0f;
        yield return new WaitUntil(() =>
        {
            timeElapsed += Time.deltaTime;
            Health = Mathf.Lerp(0f, Max_Health, timeElapsed);
            Health = Mathf.Clamp(Health, 0f, Max_Health);
            _bossHUD.UpdateLifeBar (Health, Max_Health);
            return Health == Max_Health;
        });
        initialHealth = Health;
        dead = false;
        _pathing.MoveSpeedModifier = SpeedMod;
        yield return new WaitForSeconds (MORPH.GeneralDelay);

        morphLevel++;

        //Finish
        _pathing.Stop (false);
        _isIdle = true;
        _sm.SetState (Idle);

        middleCollider.enabled = true;
        leftWingCollider.enabled = true;
        rightWingCollider.enabled = true;

        foreach (var spawnMinions in SPAWN_MINIONS)
        {
            Abilities.Remove(spawnMinions);
        }

        Abilities.Remove(BULLET_WALL);
        
        foreach (var spawnMinions in PHASE2_SPAWN_MINIONS)
        {
            spawnMinions.Action = SpawnMinions;
            spawnMinions.IgnoreCooldownCorrection = true;
            spawnMinions.Cooldown.ResetCooldown();
            Abilities.Add (spawnMinions);
        }
        Abilities.Add(BARRAGE);
        
        FindFirstObjectByType<VortexSpawner>().SpawnRate = vortexSpawnRate;
        
        PHASE2.IsEnabled = true;
    }

    public void DeathAnimation()
    {
        StopAllCoroutines();
        StartCoroutine(DeathAnimation_Exec());
    }
    public IEnumerator DeathAnimation_Exec()
    {
        var childMaterials = new List<Material> () {
            transform.Find ("Base").GetComponent<SpriteRenderer> ().material,
            transform.Find ("Wing_Left").GetComponent<SpriteRenderer> ().material,
            transform.Find ("Wing_Right").GetComponent<SpriteRenderer> ().material,
            transform.Find ("Wing_Left_ext").GetComponent<SpriteRenderer> ().material,
            transform.Find ("Wing_Right_ext").GetComponent<SpriteRenderer> ().material,
            transform.Find ("Weapon_Left").GetComponent<SpriteRenderer> ().material,
            transform.Find ("Weapon_Right").GetComponent<SpriteRenderer> ().material,
            transform.Find ("BigCannon").GetComponent<SpriteRenderer> ().material
        };
        
        var childSrs = new List<SpriteRenderer> () {
            transform.Find ("Base").GetComponent<SpriteRenderer> (),
            transform.Find ("Wing_Left").GetComponent<SpriteRenderer> (),
            transform.Find ("Wing_Right").GetComponent<SpriteRenderer> (),
            transform.Find ("Wing_Left_ext").GetComponent<SpriteRenderer> (),
            transform.Find ("Wing_Right_ext").GetComponent<SpriteRenderer> (),
            transform.Find ("Weapon_Left").GetComponent<SpriteRenderer> (),
            transform.Find ("Weapon_Right").GetComponent<SpriteRenderer> (),
            transform.Find ("BigCannon").GetComponent<SpriteRenderer> ()
        };
        
        var middleCollider = GetComponent<PolygonCollider2D> ();
        var leftWingCollider = transform.Find("Wing_Left").GetComponent<PolygonCollider2D> ();
        var rightWingCollider = transform.Find("Wing_Right").GetComponent<PolygonCollider2D> ();
        
        var shipMovement = FindFirstObjectByType<PlayerController>();
        var cameraShake = GameObject.Find("Main Camera").GetComponent<CameraShake>();
        var musicPlayer = FindFirstObjectByType<MusicPlayer>();
        var backgrounds = FindObjectsByType<ScrollingBackground>(FindObjectsInactive.Include, FindObjectsSortMode.None);
        var starfield = FindFirstObjectByType<Starfield>();
        
        middleCollider.enabled = false;
        leftWingCollider.enabled = false;
        rightWingCollider.enabled = false;
        
        _isIdle = false;
        _pathing.Stop(true);

        for (int i = 0; i < childMaterials.Count; i++)
        {
            var r = childMaterials[i].GetColor ("_Color").r;
            var g = childMaterials[i].GetColor ("_Color").g;
            var b = childMaterials[i].GetColor ("_Color").b;
            childMaterials[i].SetColor ("_Color", new Color(r,g,b,1f));
        }
        for (int i = 0; i < childSrs.Count; i++)
        {
            var c = childSrs[i].color;
            c.a = 1f;
            childSrs[i].color = c;
        }
        
        middleCollider.enabled = false;
        leftWingCollider.enabled = false;
        rightWingCollider.enabled = false;
        
        yield return new WaitForSeconds(1f);
        
        //Blink
        bool blink = true;
        for (int i = 0; i < 6; i++)
        {
            ChangeAllColor(childMaterials, blink ? MORPH.MaxAlphaValue : -MORPH.MaxAlphaValue, MORPH.MaxAlphaValue);
            blink = !blink;
            yield return new WaitForSeconds(.05f);
        }
        
        //ShineOn
        for (int i = 0; i < 40; i++)
        {
            var variation = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 2f));
            var explosion = Instantiate (PHASE2.Explosion, transform.position + variation, transform.rotation);
            Destroy (explosion, durationOfExplosion);
            AudioSource.PlayClipAtPoint (PHASE2.ExplosionSfx, Camera.main.transform.position, deathSoundVolume);
            if (i == 10)
                StartCoroutine(_pathing.MoveToCenterRoutine());
            if (i > 16)
                ChangeAllColor(childMaterials, MORPH.MorphAlphaVariation, MORPH.MaxAlphaValue);
            if(i == 20)
                cameraShake.ProgressiveShake(10f, 0.05f, 1.25f);
            
            yield return new WaitForSeconds(.15f);
        }
        
        yield return new WaitUntil (() => ChangeAllColor (childMaterials, MORPH.MorphAlphaVariation, MORPH.MaxAlphaValue));

        //Gravity
        var gravityField = Instantiate (GRAVITY.Projectile, transform.position + (new Vector3 (0, 0, 6)), Quaternion.identity);
        var gravityComp = gravityField.GetComponent<GravityField>();
        var gravitySr = gravityField.GetComponent<SpriteRenderer>();
        gravityComp.Durability = 8f;
        yield return new WaitForEndOfFrame();
        var color = Color.white;
        color.a = .75f;
        gravitySr.color = color;
        var pos = gravityField.transform.position;
        pos.z = -1f;
        gravityField.transform.position = pos;
        GRAVITY.SetShoot(gravityField);
        gravityField.transform.localScale = new Vector3(.5f, .5f);

        //sound
        var audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = PHASE2.FadeSfx;
        audioSource.time = 1.25f;
        audioSource.volume = deathSoundVolume;
        audioSource.Play();
        
        
        //flashbang
        var oldColor = gravitySr.color;
        var oldSize = gravityField.transform.localScale;
        
        var newColor = Color.white;
        newColor.a = 0f;
        gravitySr.color = newColor;
        gravityField.transform.localScale = new Vector3(GRAVITY.MaxSize, GRAVITY.MaxSize);
        newColor.a = 1f;
        gravitySr.color = newColor;
        yield return new WaitForSeconds(.1f);
        newColor.a = 0f;
        gravitySr.color = newColor;

        gravityField.transform.localScale = oldSize;
        gravitySr.color = oldColor;
        
        //increase gravity
        yield return new WaitUntil(() =>
        {
            if (gravityField.transform.localScale.x > 30f)
            {
                var color = gravitySr.color;
                color.a += .005f;
                color.a = Mathf.Clamp(color.a, 0f, 1f);
                gravitySr.color = color;
            }

            GRAVITY.IncreaseShoot(.15f);
            return gravityField.transform.localScale.x >= GRAVITY.MaxSize && gravitySr.color.a == 1f;
        });
        
        shipMovement.LockMovement(true);
        StartCoroutine(shipMovement.MovePlayerToCenter());

        yield return new WaitForSeconds(2f);

        var bg = backgrounds.FirstOrDefault(_ => _.name.ToLower().Contains("red"))
                 ?? backgrounds.FirstOrDefault(_ => _.name.ToLower().Contains("green"))
                 ?? backgrounds.FirstOrDefault(_ => _.name.ToLower().Contains("pink"))
                 ?? backgrounds.FirstOrDefault(_ => _.name.ToLower().Contains("blue"));

        bg?.gameObject.SetActive(true);
        bg?.RestoreMaxSpeed();
        
        musicPlayer.FadeIn(2f);
        starfield.ResetSettings();

        deathAnimationPlayed = true;

        yield return new WaitForSeconds(3f);
        ForceDeath();
    }
}