﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityField : MonoBehaviour {
	public float Durability;
	public float VanishSpeed;
	[HideInInspector]public bool DontApplyGravity;

	void Start ()
	{
		var diablo = FindFirstObjectByType<Diablo>();
		if (diablo && diablo.PHASE2.IsEnabled)
		{
			var sr = GetComponent<SpriteRenderer>();
			var color = new Color(.2f, .1f, 0f, sr.color.a);
			sr.color = color;
		}
		StartCoroutine (Deathtime ());
	}

	IEnumerator Deathtime () {

		if (!DontApplyGravity)
		{
			var cameraShake = GameObject.Find("Main Camera").GetComponent<CameraShake>();
			cameraShake.ProgressiveShake(Durability * 1.3f, 0f, 1f);
		}

		yield return new WaitForSeconds (Durability);

		var spriteRenderer = GetComponent<SpriteRenderer> ();

		yield return new WaitUntil (() => {
			var alpha = Mathf.Clamp01 (spriteRenderer.color.a - VanishSpeed);
			spriteRenderer.color = new Color (spriteRenderer.color.r, spriteRenderer.color.g, spriteRenderer.color.b, alpha);

			return spriteRenderer.color.a == 0;
		});

		Destroy (gameObject);
	}
}