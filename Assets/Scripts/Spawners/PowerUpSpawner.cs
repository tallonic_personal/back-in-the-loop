﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class PowerUpSpawner : MonoBehaviour {
    public float dropRate;

    public List<GameObject> drops;

    public ShipRank _shipRank;
    public EnemySpawner _enemySpawner;
    
    public const int RexStagePwLimit = 8;
    public const int MummyStagePwLimit =  10;
    public const int AlastorStagePwLimit =  12;

    public bool CanSpawnUpgradePowerUps => _enemySpawner.IsDiabloStage
                                           || _enemySpawner.IsRexStage && _shipRank.GetCurrentLevel() <= RexStagePwLimit
                                           || _enemySpawner.IsMummyStage && _shipRank.GetCurrentLevel() <= MummyStagePwLimit
                                           || _enemySpawner.IsAlastorStage && _shipRank.GetCurrentLevel() <= AlastorStagePwLimit;

    private void Start()
    {
        _shipRank = FindFirstObjectByType<ShipRank>();
        _enemySpawner = FindFirstObjectByType<EnemySpawner>(FindObjectsInactive.Include);
    }

    public void Spawn (Vector3 position) {
        var spawnList = GetSpawnList ();
        if (spawnList == null || spawnList.All(_ => _.Value == 0f)) return;

        var random = Random.Range (0, 100);
        var spawnType = GetSpawnType (random, spawnList);
        CreatePowerUp (spawnType, position);
    }

    void CreatePowerUp (ShipType type, Vector3 position) {
        if (type == ShipType.Normal)
            Instantiate (drops[0], position, Quaternion.identity);
        if (type == ShipType.Splitter)
            Instantiate (drops[1], position, Quaternion.identity);
        if (type == ShipType.Blaster)
            Instantiate (drops[2], position, Quaternion.identity);
        if (type == ShipType.X)
        {
            if (_shipRank.currentRank.Level < 10 && !FindFirstObjectByType<PowerUp>())
            {
                Instantiate(drops[3], position, Quaternion.identity);
            }
        }
    }

    IDictionary<ShipType, float> GetSpawnList () {
        if (_shipRank == null) return null;

        var powerUpList = new Dictionary<ShipType, float>()
        {
            { ShipType.Normal , 0f},
            { ShipType.Splitter , 0f},
            { ShipType.Blaster , 0f},
            { ShipType.X , 0f}
        };
        
        var ranks = _shipRank.Ranks;
        var ranksNoX = _shipRank.Ranks.Where(_ => _.ShipType != ShipType.X).ToArray();

        if (ranks.All(_ => _.IsMaximized)) return powerUpList;
        
        if (_enemySpawner.IsDiabloStage)
        {
            if (ranks.Where(_ => _.ShipType != ShipType.X).All(_ => _.Level >= 4))
            {
                powerUpList[ShipType.X] = 100f;
                return powerUpList;
            }
        }

        if (ranksNoX.All(_ => _.IsMaximized))
        {
            foreach (var rank in ranksNoX)
            {
                if (rank.ShipType != _shipRank.currentRank.ShipType)
                    powerUpList[rank.ShipType] += 50f;
            }

            return powerUpList;
        }

        //se o meu não estiver maximizado, não aumenta o meu
        //se houver algum não maximizado
        
        if (_shipRank.currentRank.IsMaximized || !CanSpawnUpgradePowerUps)
        {
            var ranksToDist = ranksNoX.Where(_ => _.ShipType != _shipRank.currentRank.ShipType).ToArray();
            foreach (var rank in ranksToDist)
            {
                powerUpList[rank.ShipType] += 100f / ranksToDist.Length;
            }
        }
        else
        {
            powerUpList[_shipRank.currentRank.ShipType] += 25f;
            
            var ranksToDist = ranksNoX.Where(_ => _.Level < 4f).ToArray();
            foreach (var rank in ranksToDist)
            {
                powerUpList[rank.ShipType] += 75f / ranksToDist.Length;
            }
        }
        
        // var blueText = $"<color=lightblue>{powerUpList[ShipType.Normal]}</color>";
        // var greenText = $"<color=green>{powerUpList[ShipType.Splitter]}</color>";
        // var redText = $"<color=red>{powerUpList[ShipType.Blaster]}</color>";
        // var orangeText = $"<color=orange>{powerUpList[ShipType.X]}</color>";
        // Debug.Log($"Drop Chance: {blueText} {greenText} {redText} {orangeText}");

        return powerUpList;
    }

    ShipType GetSpawnType (float random, IDictionary<ShipType, float> list)
    {
        var spawnTypesToSpawn = list.Where(_ => _.Value > 0f).ToArray();

        var acc = 0f;
        foreach (var spawnTypeData in spawnTypesToSpawn)
        {
            acc += spawnTypeData.Value;
            if (random <= acc) {
                return spawnTypeData.Key;
            }
        }

        Debug.LogError ("ShipType not found.");
        return (ShipType) 4;
    }
}