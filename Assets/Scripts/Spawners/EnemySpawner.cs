﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    public List<WaveConfig> AllWavesConfiguration;
    public List<GameObject> AllEnemiesConfiguration;
    public ShipRank PlayerRank;

    public List<WaveConfig> WaveConfigs;
    private Dictionary<int, List<WaveConfig>> WaveConfigsByLevel;
    int startWave;
    int enemiesAmount = 84;

    [HideInInspector]
    public bool SpawnDone;

    public event System.Action<int, int> OnWaveSpawned = delegate { }; //waveCount, totalWaves
    public event System.Action OnBossKilled = delegate { };
    
    public int BossesKilled;
    public bool IsRexStage => BossesKilled == 0;
    public bool IsMummyStage => BossesKilled == 1;
    public bool IsAlastorStage => BossesKilled == 2;
    public bool IsDiabloStage => BossesKilled == 3;
    
    GameProgressionHUD _gameProgression;
    
    [SerializeField] private SpawnControl _spawnControl;
    
    void Start () {
        WaveConfigs = GetWaves ();
        WaveConfigsByLevel = SeparateWavesByLevel(WaveConfigs.ToArray());

        startWave = 0;
        StartCoroutine (SpawnAllWaves ());

        _gameProgression = GameObject.Find ("UI").GetComponent<GameProgressionHUD> ();
    }

    void OnEnable () {
        if (WaveConfigs.Count > 0)
            StartCoroutine (SpawnAllWaves ());
    }

    List<WaveConfig> GetWaves () {
        //waves
        var basicWaves = AllWavesConfiguration.GetRange (0, 8);
        var repeatableWaves = AllWavesConfiguration.GetRange (8, 8);
        var bossWaves = AllWavesConfiguration.GetRange (16, 4);

        var waves = new List<WaveConfig> ();

        //FLAGS
        var insertIndexes = new List<int> ();

        var repFlag = 0;
        var insertRepIndexes = new List<int> ();

        var bossFlag = 0;
        var currentBossIndex = 0;

        //ALGORITHM
        for (int i = 1; i <= enemiesAmount; i++) {
            repFlag++;
            bossFlag++;

            //BOSS
            if (bossFlag == 21) {
                waves.Add (bossWaves[currentBossIndex]);
                currentBossIndex++;

                repFlag = 0;
                bossFlag = 0;

                continue;
            }

            //REPEATABLE
            if (repFlag == 5) {
                var repRandomIndex = -1;

                do
                    repRandomIndex = Random.Range (0, repeatableWaves.Count);
                while (insertRepIndexes.Any (_ => _ == repRandomIndex));

                waves.Add (repeatableWaves[repRandomIndex]);
                insertRepIndexes.Add (repRandomIndex);

                repFlag = 0;

                if (insertRepIndexes.Count == repeatableWaves.Count) insertRepIndexes.Clear ();

                continue;
            }

            //NORMAL
            var randomIndex = -1;

            do
                randomIndex = Random.Range (0, basicWaves.Count);
            while (insertIndexes.Any (_ => _ == randomIndex));

            waves.Add (basicWaves[randomIndex]);
            insertIndexes.Add (randomIndex);

            if (insertIndexes.Count == basicWaves.Count) insertIndexes.Clear ();
        }

        return waves;
    }

    Dictionary<int, List<WaveConfig>> SeparateWavesByLevel(WaveConfig[] waveConfigs)
    {
        var wavesByLevel = new Dictionary<int, List<WaveConfig>>();
        
        var level = 1;

        foreach (var wave in waveConfigs)
        {
            if (!wavesByLevel.ContainsKey(level))
                wavesByLevel.Add(level, new List<WaveConfig>());
            
            wavesByLevel[level].Add(wave);
            if (wave.IsBoss) level++;
        }

        return wavesByLevel;    
    }

    IEnumerator SpawnAllWaves () {
        var enemieEnergy = EnemieEnergy ();
        yield return new WaitForSeconds (0.01f);

        for (int i = startWave; i < WaveConfigs.Count; i++) {
            /*
                Normal/Splitter/Strong Ranks - Weak = 1 ; Normal = 2 3 ; Strong = 4
                X Ranks - Weak = 1 2 3 ; Normal = 4 5 6 7 ; Strong = 8 9 10
             */
            var playerIsX = PlayerRank.currentRank.ShipType == ShipType.X;
            var enemyLevel = System.Convert.ToInt32 (PlayerRank.currentRank.Level / (playerIsX ? 4 : 2));
            var playerDifficultyRatio = (PlayerRank.GetCurrentLevel () / 6f) + 1;

            var wave = WaveConfigs[i];

            /*test area */
            var validation = false;
            //validation = wave.IsBoss; //JUST SHIPS
            //validation = !wave.IsBoss; //JUST BOSS
            //validation = !wave.IsBoss && !wave.RepeatPathing; //BOSS/REPEATABLES
            //  validation = !wave.EnemyPrefab.name.Contains ("Diablo"); //SPECIFIC BOSS
            
            if (wave.IsBoss && !_spawnControl.HasFlag(SpawnControl.Boss)
                || !wave.IsBoss && (
                    wave.RepeatPathing && !_spawnControl.HasFlag(SpawnControl.RepeatPathing)
                    || !wave.IsBoss && !wave.RepeatPathing && !_spawnControl.HasFlag(SpawnControl.Normal)
                )
                ) {
                startWave++;
                continue;
            }
            /*test area */

            var energy = 0;

            if (wave.IsBoss) {
                wave.TimeBetweenWaves = 600;
                wave.SpawnRandomFactor = 0;
                wave.NumberOfEnemies = 1;
                wave.MoveSpeed = 4;
                //energy = 10;
                energy = enemieEnergy.Where (_ =>
                    wave.EnemyPrefab.name.Contains (_.EnemyType) &&
                    _.ShipType == PlayerRank.currentRank.ShipType &&
                    _.Level == PlayerRank.currentRank.Level).FirstOrDefault ().Energy;
            } else if (wave.RepeatPathing) {
                wave.TimeBetweenWaves = 10;
                wave.SpawnRandomFactor = 1;
                wave.NumberOfEnemies = 1 * (int) playerDifficultyRatio;
                wave.MoveSpeed = Random.Range (5f, 5f) * (playerDifficultyRatio / 2);
                var pathDirection = new Vector3 (0f, Random.Range (0f, 1f) >= 0.5f ? 0 : 180, 0f);
                wave.PathPrefab.transform.Rotate (pathDirection);
                var enemyIndex = Random.Range (3, 5);
                //var enemyIndex = 3; //specific ship
                wave.EnemyPrefab = AllEnemiesConfiguration[enemyIndex + (5 * enemyLevel)];

                energy = enemieEnergy.Where (_ =>
                    wave.EnemyPrefab.name.Contains (_.EnemyType) &&
                    _.ShipType == PlayerRank.currentRank.ShipType &&
                    _.Level == PlayerRank.currentRank.Level).FirstOrDefault ().Energy * 2;
            } else {
                wave.TimeBetweenWaves = Random.Range (3, 8);
                wave.SpawnRandomFactor = Random.Range (0.1f, 1.5f);
                wave.NumberOfEnemies = Random.Range (3, 8);
                wave.MoveSpeed = Random.Range (3f, 5f) * (playerDifficultyRatio / 2);
                var pathDirection = new Vector3 (0f, Random.Range (0f, 1f) >= 0.5f ? 0 : 180, 0f);
                wave.PathPrefab.transform.Rotate (pathDirection);
                var enemyIndex = Random.Range (0, 3);
                //var enemyIndex = 4; //specific ship
                wave.EnemyPrefab = AllEnemiesConfiguration[enemyIndex + (5 * enemyLevel)];

                energy = enemieEnergy.Where (_ =>
                    wave.EnemyPrefab.name.Contains (_.EnemyType) &&
                    _.ShipType == PlayerRank.currentRank.ShipType &&
                    _.Level == PlayerRank.currentRank.Level).FirstOrDefault ().Energy;

                if (PlayerRank.currentRank.ShipType != ShipType.X)
                {
                    if (PlayerRank.currentRank.Level <= 2)
                    {
                        var ranksAtFour = PlayerRank.Ranks.Where(_ => _.Level == 4 && _ != PlayerRank.currentRank);
                        energy = Mathf.FloorToInt(energy * (1 - ranksAtFour.Count() * .25f));    
                    }
                }
            }

            startWave++;
            _gameProgression.UpdateProgressionBar (i + 1, enemiesAmount);

            yield return StartCoroutine (SpawnWave (wave, energy, i + 1));
        }

        yield return new WaitUntil (() => {
            var enemies = GameObject.FindGameObjectsWithTag ("Enemy");
            var bosses = GameObject.FindGameObjectsWithTag ("Boss");

            return enemies.Length == 0 && bosses.Length == 0;
        });

        //GameObject.Find ("StageController").GetComponent<StageController> ().Fanfare ();
    }

    IEnumerator SpawnWave (WaveConfig waveConfig, int energy, int waveNumber) {
        GameObject enemy = null;
        List<GameObject> enemies = new List<GameObject>();

        if (waveConfig.IsBoss) {
            yield return new WaitUntil (() => GameObject.FindGameObjectsWithTag ("Enemy").Count () == 0);
        }

        var waveIndexData = WaveConfigsByLevel.First(_ => _.Value.Contains(waveConfig));
        OnWaveSpawned(waveIndexData.Value.IndexOf(waveConfig) + 1, waveIndexData.Value.Count);
        
        for (int i = 0; i < waveConfig.NumberOfEnemies; i++) {
            enemy = Instantiate (
                waveConfig.EnemyPrefab,
                waveConfig.Waypoints () [0].position,
                Quaternion.identity);
            enemy.GetComponent<EnemyPathing> ().SetWaveConfig (waveConfig);
            var enemyComp = enemy.GetComponent<Enemy>();
            enemyComp.Health = energy;
            enemyComp.WaveNumber = waveNumber;
            enemies.Add(enemy);

            yield return new WaitForSeconds (waveConfig.SpawnRandomFactor);
        }
        
        if (waveConfig.RepeatPathing || waveConfig.IsBoss) {
            if (waveConfig.IsBoss) {
                GameObject.Find ("StageController").GetComponent<StageController> ().BossEncounter ();
            }

            var timer = 0f;
            yield return new WaitUntil (() => {
                timer += Time.deltaTime;
                return enemy == null || timer >= waveConfig.TimeBetweenWaves;
            });
        } else {
            var timer = 0f;
            yield return new WaitUntil (() => {
                timer += Time.deltaTime;
                enemies.RemoveAll(_ => _ == null);
                return !enemies.Any() || timer >= waveConfig.TimeBetweenWaves;
            });

            if (enemies.Any())
                yield return new WaitForSeconds(.5f);
        }

        //yield return new WaitForSeconds (0f);
    }

    public IEnumerable<EnemyLevel> EnemieEnergy () {
        var enemyLevels = new List<EnemyLevel> ();
        enemyLevels.AddRange (GetBoomerLevels ());
        enemyLevels.AddRange (GetDodgerLevels ());
        enemyLevels.AddRange (GetGunnerLevels ());
        enemyLevels.AddRange (GetJetLevels ());
        enemyLevels.AddRange (GetWarriorLevels ());
        enemyLevels.AddRange (GetBossesLevels ());
        return enemyLevels;
    }

    IEnumerable<EnemyLevel> GetBoomerLevels () {
        var boomerLevels = new List<EnemyLevel> ();
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.Normal, 1, 25));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.Normal, 2, 50));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.Normal, 3, 80));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.Normal, 4, 120));
        
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.Splitter, 1, 10));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.Splitter, 2, 25));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.Splitter, 3, 50));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.Splitter, 4, 100));
        
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.Blaster, 1, 20));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.Blaster, 2, 40));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.Blaster, 3, 60));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.Blaster, 4, 100));
        
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.X, 1, 30));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.X, 2, 40));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.X, 3, 60));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.X, 4, 100));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.X, 5, 150));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.X, 6, 180));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.X, 7, 230));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.X, 8, 270));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.X, 9, 320));
        boomerLevels.Add (new EnemyLevel ("Boomer", ShipType.X, 10, 350));
        return boomerLevels;
    }
    IEnumerable<EnemyLevel> GetDodgerLevels () {
        var dodgerLevels = new List<EnemyLevel> ();
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.Normal, 1, 20));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.Normal, 2, 40));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.Normal, 3, 70));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.Normal, 4, 120));
        
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.Splitter, 1, 10));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.Splitter, 2, 25));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.Splitter, 3, 50));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.Splitter, 4, 95));
        
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.Blaster, 1, 20));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.Blaster, 2, 60));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.Blaster, 3, 100));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.Blaster, 4, 160));
        
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.X, 1, 30));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.X, 2, 40));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.X, 3, 60));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.X, 4, 100));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.X, 5, 150));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.X, 6, 180));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.X, 7, 230));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.X, 8, 270));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.X, 9, 320));
        dodgerLevels.Add (new EnemyLevel ("Dodger", ShipType.X, 10, 350));
        return dodgerLevels;
    }
    IEnumerable<EnemyLevel> GetGunnerLevels () {
        var gunnerLevels = new List<EnemyLevel> ();
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.Normal, 1, 20));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.Normal, 2, 30));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.Normal, 3, 80));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.Normal, 4, 160));
        
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.Splitter, 1, 10));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.Splitter, 2, 20));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.Splitter, 3, 35));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.Splitter, 4, 70));
        
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.Blaster, 1, 20));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.Blaster, 2, 40));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.Blaster, 3, 60));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.Blaster, 4, 100));
        
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.X, 1, 30));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.X, 2, 40));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.X, 3, 60));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.X, 4, 100));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.X, 5, 150));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.X, 6, 180));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.X, 7, 230));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.X, 8, 270));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.X, 9, 320));
        gunnerLevels.Add (new EnemyLevel ("Gunner", ShipType.X, 10, 350));
        return gunnerLevels;
    }
    IEnumerable<EnemyLevel> GetJetLevels () {
        var jetLevels = new List<EnemyLevel> ();
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.Normal, 1, 25));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.Normal, 2, 50));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.Normal, 3, 80));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.Normal, 4, 120));
        
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.Splitter, 1, 10));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.Splitter, 2, 25));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.Splitter, 3, 50));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.Splitter, 4, 100));
        
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.Blaster, 1, 20));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.Blaster, 2, 40));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.Blaster, 3, 60));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.Blaster, 4, 100));
        
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.X, 1, 30));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.X, 2, 40));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.X, 3, 60));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.X, 4, 100));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.X, 5, 150));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.X, 6, 180));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.X, 7, 230));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.X, 8, 270));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.X, 9, 320));
        jetLevels.Add (new EnemyLevel ("Jet", ShipType.X, 10, 350));
        return jetLevels;
    }
    IEnumerable<EnemyLevel> GetWarriorLevels () {
        var warriorLevels = new List<EnemyLevel> ();
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.Normal, 1, 20));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.Normal, 2, 30));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.Normal, 3, 80));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.Normal, 4, 160));
        
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.Splitter, 1, 10));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.Splitter, 2, 20));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.Splitter, 3, 35));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.Splitter, 4, 70));
        
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.Blaster, 1, 20));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.Blaster, 2, 40));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.Blaster, 3, 60));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.Blaster, 4, 100));
        
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.X, 1, 30));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.X, 2, 40));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.X, 3, 60));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.X, 4, 100));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.X, 5, 150));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.X, 6, 180));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.X, 7, 230));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.X, 8, 270));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.X, 9, 320));
        warriorLevels.Add (new EnemyLevel ("Warrior", ShipType.X, 10, 350));
        return warriorLevels;
    }
    IEnumerable<EnemyLevel> GetBossesLevels () {
        var bossLevels = new List<EnemyLevel> ();
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.Normal, 1, 1200));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.Normal, 2, 2500));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.Normal, 3, 6200));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.Normal, 4, 10000));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.Splitter, 1, 600));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.Splitter, 2, 1250));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.Splitter, 3, 1700));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.Splitter, 4, 5000));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.Blaster, 1, 1100));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.Blaster, 2, 3400));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.Blaster, 3, 6200));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.Blaster, 4, 12000));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.X, 1, 2500));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.X, 2, 3000));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.X, 3, 3500));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.X, 4, 4000));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.X, 5, 5500));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.X, 6, 6500));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.X, 7, 7500));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.X, 8, 9000));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.X, 9, 10500));
        bossLevels.Add (new EnemyLevel ("R.E.X", ShipType.X, 10, 12000));

        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.Normal, 1, 1000));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.Normal, 2, 1800));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.Normal, 3, 3750));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.Normal, 4, 8000));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.Splitter, 1, 400));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.Splitter, 2, 750));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.Splitter, 3, 1000));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.Splitter, 4, 3000));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.Blaster, 1, 860));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.Blaster, 2, 2750));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.Blaster, 3, 4000));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.Blaster, 4, 7000));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.X, 1, 1000));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.X, 2, 1500));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.X, 3, 1750));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.X, 4, 2000));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.X, 5, 2500));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.X, 6, 3000));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.X, 7, 3750));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.X, 8, 4500));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.X, 9, 5000));
        bossLevels.Add (new EnemyLevel ("Mummy", ShipType.X, 10, 6000));

        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.Normal, 1, 1100));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.Normal, 2, 2500));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.Normal, 3, 5000));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.Normal, 4, 8000));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.Splitter, 1, 400));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.Splitter, 2, 750));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.Splitter, 3, 1000));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.Splitter, 4, 3000));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.Blaster, 1, 900));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.Blaster, 2, 2500));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.Blaster, 3, 4000));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.Blaster, 4, 7000));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.X, 1, 3750));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.X, 2, 4500));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.X, 3, 5250));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.X, 4, 6000));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.X, 5, 8250));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.X, 6, 9750));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.X, 7, 11250));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.X, 8, 13500));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.X, 9, 15750));
        bossLevels.Add (new EnemyLevel ("Alastor", ShipType.X, 10, 18000));

        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.Normal, 1, 3000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.Normal, 2, 5000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.Normal, 3, 10000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.Normal, 4, 20000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.Splitter, 1, 2000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.Splitter, 2, 5000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.Splitter, 3, 7000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.Splitter, 4, 10000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.Blaster, 1, 3000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.Blaster, 2, 5000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.Blaster, 3, 10000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.Blaster, 4, 20000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.X, 1, 15000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.X, 2, 15000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.X, 3, 15000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.X, 4, 15000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.X, 5, 15000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.X, 6, 20000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.X, 7, 20000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.X, 8, 20000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.X, 9, 20000));
        bossLevels.Add (new EnemyLevel ("Diablo", ShipType.X, 10, 20000));
        return bossLevels;
    }

    public class EnemyLevel {
        public string EnemyType;
        public ShipType ShipType;
        public int Level;
        public int Energy;

        public EnemyLevel (string enemyType, ShipType shipType, int level, int energy) {
            EnemyType = enemyType;
            ShipType = shipType;
            Level = level;
            Energy = energy;
        }
    }
    
    [System.Flags]
    public enum SpawnControl
    {
        None = 0,
        Normal = 1,
        RepeatPathing = 2,
        Boss = 4
    }
}