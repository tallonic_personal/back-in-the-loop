﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackground : MonoBehaviour
{
    public float MinSpeed = 0.2f;
    public float MaxSpeed = 0.4f;
    public float BossSpeed = 0.8f;
#pragma warning disable CS0414 // Field is assigned but its value is never used
    private bool _alreadyFinishStage;
#pragma warning restore CS0414 // Field is assigned but its value is never used
    Material myMaterial;
    public Vector2 offset;

    private ScrollingBackground _previousBackground;

    void Start()
    {
        myMaterial = GetComponent<Renderer>().material;
        offset = new Vector2(0f, MinSpeed);
    }

    private void OnEnable()
    {
        var spawner = FindFirstObjectByType<EnemySpawner>(FindObjectsInactive.Include);
        if (spawner)
        {
            spawner.OnWaveSpawned += UpdateSpeed;
            spawner.OnBossKilled += RestoreMaxSpeed;
        }
    }

    private void OnDisable()
    {
        var spawner = FindFirstObjectByType<EnemySpawner>(FindObjectsInactive.Include);
        if (spawner)
        {
            spawner.OnWaveSpawned -= UpdateSpeed;
            spawner.OnBossKilled -= RestoreMaxSpeed;
        }
            
    }

    void Update()
    {
        myMaterial.mainTextureOffset += offset * Time.deltaTime;
    }

    private void UpdateSpeed(int waveCount, int totalOfWaves)
    {
        float targetSpeed = 0f;
        if (waveCount == totalOfWaves) //check if is boss
        {
            targetSpeed = BossSpeed;
            _alreadyFinishStage = true;
        }
        else
        {
            var interval = (float)waveCount / (totalOfWaves - 1);
            targetSpeed = Mathf.Lerp(MinSpeed, MaxSpeed, interval);
        }

        StartCoroutine(ChangeSpeed(targetSpeed, 2f));
    }

    public void RestoreMaxSpeed()
    {
        Debug.Log("Restoring");
        StartCoroutine(ChangeSpeed(MaxSpeed, 2f));
    }

    private IEnumerator ChangeSpeed(float targetSpeed, float duration)
    {
        var startSpeed = offset.y;
        var time = 0f;
        while (time < duration)
        {
            time += Time.deltaTime;
            offset = new Vector2(0f, Mathf.Lerp(startSpeed, targetSpeed, time / duration));
            if (_previousBackground)
                _previousBackground.ForceChangeSpeed(offset.y);
            yield return null;
        }
    }

    private void ForceChangeSpeed(float ySpeed)
    {
        offset = new Vector2(0f, ySpeed);
    }

    public void SetPreviousBackground(ScrollingBackground previousBackground)
    {
        _previousBackground = previousBackground;
    }
}