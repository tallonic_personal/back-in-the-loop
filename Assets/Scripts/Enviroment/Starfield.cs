using System;
using System.Collections;
using UnityEngine;

namespace Enviroment
{
    public class Starfield : MonoBehaviour
    {
        [SerializeField] private ParticleSystem closeStarfield;
        [SerializeField] private ParticleSystem farStarfield;

        private StarfieldData closeInitialData;
        private StarfieldData farInitialData;

        private void Start()
        {
            var main = closeStarfield.main;
            var emission = closeStarfield.emission;
            
            closeInitialData = new StarfieldData
            {
                MinSpeed = main.startSpeed.constantMin,
                MaxSpeed = main.startSpeed.constantMax,
                Rate = emission.rateOverTime.constant,
                Lifetime = main.startLifetime.constant
            };
            
            main = farStarfield.main;
            emission = farStarfield.emission;
            
            farInitialData = new StarfieldData
            {
                MinSpeed = main.startSpeed.constantMin,
                MaxSpeed = main.startSpeed.constantMax,
                Rate = emission.rateOverTime.constant,
                Lifetime = main.startLifetime.constant
            };
        }

        public void ChangeStarfieldToBossMode()
        {
            var closeData = new StarfieldData
            {
                MinSpeed = 30f,
                MaxSpeed = 40f,
                Rate = 50,
                Lifetime = 1f
            };
            
            StartCoroutine(ChangeStarField(closeStarfield, closeData));
            
            var farData = new StarfieldData
            {
                MinSpeed = 10f,
                MaxSpeed = 20f,
                Rate = 35,
                Lifetime = 1.5f
            };
            StartCoroutine(ChangeStarField(farStarfield, farData));
        }

        public void ChangeStarfieldToBossMode2()
        {
            var closeData = new StarfieldData
            {
                MinSpeed = 50f,
                MaxSpeed = 60f,
                Rate = 150,
                Lifetime = .5f
            };
            StartCoroutine(ChangeStarField(closeStarfield, closeData));
            
            var farData = new StarfieldData
            {
                MinSpeed = 30f,
                MaxSpeed = 40f,
                Rate = 75,
                Lifetime = .75f
            };
            StartCoroutine(ChangeStarField(farStarfield, farData));
        }

        private IEnumerator ChangeStarField(ParticleSystem particleSystem, StarfieldData data)
        {
            var main = particleSystem.main;
            var emission = particleSystem.emission;

            main.simulationSpeed = 1f;

            var timeElapsed = 0f;
            while (timeElapsed < 8f)
            {
                timeElapsed += Time.deltaTime;
                if (timeElapsed <= 4f)
                {
                    var minSpd = Mathf.Lerp(data.MinSpeed / 2f, data.MinSpeed, timeElapsed / 4f);
                    var maxSpd = Mathf.Lerp(data.MaxSpeed / 2f, data.MaxSpeed, timeElapsed / 4f);
                    main.startSpeed = new ParticleSystem.MinMaxCurve(minSpd, maxSpd);
                }

                emission.rateOverTime = Mathf.Lerp(0f, data.Rate, timeElapsed / 8f);

                yield return null;
            }

            main.startLifetime = data.Lifetime;
        }

        public void StopParticles()
        {
            var main = closeStarfield.main;
            main.simulationSpeed = 0f;
            closeStarfield.Clear();

            main = farStarfield.main;
            main.simulationSpeed = 0f;
            farStarfield.Clear();
        }

        public void ResetSettings()
        {
            StopParticles();
            StartCoroutine(ChangeStarField(closeStarfield, closeInitialData));
            StartCoroutine(ChangeStarField(farStarfield, farInitialData));
        }
    }

    public class StarfieldData
    {
        public float MinSpeed {get;set;}
        public float MaxSpeed {get;set;}
        public float Rate {get;set;}
        public float Lifetime {get;set;}
    }
}